/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright (C) 2014  Wayne Branagh
*/

package com.wbranagh.jira.plugins;

import com.opensymphony.workflow.Validator;
import com.opensymphony.module.propertyset.PropertySet;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.opensymphony.workflow.InvalidInputException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.wbranagh.jira.plugins.PluginUtility;


/**
 * Implementation of a String check validator. The validator allows you to require string
 * input to match a regexp, be null or not, or be valid xhtml.
 * 
 * @author Wayne Branagh
 * 
 */
public class StringFormats implements Validator
{
    Issue issue=null;
    PluginUtility util=new PluginUtility();
    
 
    public StringFormats() {;}
    
    /**
     * Implements the validator logic. When a user tries to advance the workflow, 
     * CF's are checked to see if they meet the conditions.
     * 
     * @param transientVars 	Map populated by Jira (eg. current ticket CF values)
     * @param args 		Map of values from the input velocity form
     * @param ps	PropertySet	 
     */
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException
    {
    	if (transientVars==null||args==null||ps==null)
    	{
    		util.buildError(null, "Internal error in validate. Missing some parameters in the call.");
    		if (util.invInpExcep!=null) throw util.invInpExcep;
    	}
    	
    	CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
    	Issue currIssue = (Issue) transientVars.get("issue");
    	if (currIssue==null)
    	{
    		util.buildError(null, "Internal error in validate. No issue handle.");
    		if (util.invInpExcep!=null) throw util.invInpExcep;
    	}
  
    	Boolean bRettype=new Boolean("true");
    	String cfNameId=(String)util.getFormData(args,"field.customFieldNameId", "");
        String cfName=util.getFieldNameFromID(customFieldManager,cfNameId);
                
    	Object objValue=util.getCustomFieldValueFromIssueById(currIssue,cfNameId);
    	// check the inputs
    	String strValue=(String)objValue;
    	Boolean checkCanBeNull=(Boolean)util.getFormData(args,"field.checkCanBeNull",bRettype);
    	if (strValue==null)
    	{
    		if ((checkCanBeNull!=null)&&(checkCanBeNull.booleanValue()))
    			return; //all is well. Nothing expected. Nothing there.
    		else
    			util.buildError(null, "Error: Expected a value for "+cfName+". Nothing entered.");
    	}
   
    	Boolean checkXhtml=(Boolean)util.getFormData(args,"field.checkXhtml",bRettype);
		if ((checkXhtml!=null)&&(checkXhtml.booleanValue()))
		{
			if (!util.isXhtmlValid(strValue))
			{
				util.buildError(null, "Error: Invalid xhtml in "+cfName+" provided.");
			}
		}

		String strinpRegExp=(String)util.getFormData(args,"field.inpRegExp", "");
		if (!util.isRegExpValid(strinpRegExp))
		{
			util.buildError(null, "Error: Invalid regexp in "+cfName+" provided for matching string regexp.");
		}
		
		if (util.invInpExcep!=null) throw util.invInpExcep;
		
         boolean isValidString=false;
         if ((strinpRegExp!=null)&&(strinpRegExp.length()>0) )
         {
        	 isValidString=strValue.matches(strinpRegExp);
         }
         
         
         if (!isValidString)
         {
        	 util.buildError(null, "Error: Validation on string value: "+strValue+" for field:"+cfName+" failed.");
        	 if (util.invInpExcep!=null) throw util.invInpExcep;
         }
        	 
    } // proc
    
} // class
