/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright (C) 2014  Wayne Branagh
*/
package com.wbranagh.jira.plugins;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.opensymphony.workflow.InvalidInputException;

/**
 * Generic utility class for Jira plugins for accessing fields and cleaning inputs
 * 
 * @author Wayne Branagh
 *
 */
public class PluginUtility 
{
	public InvalidInputException invInpExcep = null;
	
	
	/**
	 * Default public constructor
	 */
	public PluginUtility()
	{
	}
	
	 /**
     * Given an issue and the ID of a custom field, return the value of it
     * 
     * @param 	inputIssue	Current Jira issue being examined
     * @param 	cfNameId	ID of the Customfield
     * @return null if error or invalid cf ID, value of the field otherwise
     *  
     */
    public Object getCustomFieldValueFromIssueById(Issue inputIssue, String cfNameId)
    {
    	if ((cfNameId!=null)&&(cfNameId.length()>0))
    	{
            CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
            // get customfield
            CustomField customField = customFieldManager.getCustomFieldObject(cfNameId);
            if (customField != null)
            {
                Object cfValue= (Object)inputIssue.getCustomFieldValue(customField) ; 
                return cfValue;
            }
    	}
    	return null;
    }
    
    /**
     * Given a CF ID, return the name. 
     * Why not just use names instead of IDs? CF Names don't have to be unique in Jira.
     * 
     * @param customFieldId
     * @return display name of the field
     */
    public String getFieldNameFromID(CustomFieldManager customFieldManager, String customFieldId)
    {
    	if ((customFieldId==null)||(customFieldId.length()<1))
    		return null;
    	
        List<CustomField> fields=customFieldManager.getCustomFieldObjects();
        for (int a=0;a<fields.size();a++)
        {
      	  CustomField cf=fields.get(a);
      	  if (cf.getId().indexOf(customFieldId)==0)
      		  return cf.getName(); 
        }
    	return null;
    }
    
    /**
     * Remove any whitespace, dashes, underscores from the input. Null returns null
     * 
     * @param strValue	Input to clean
     * @return returns the cleaned up string or null if that's what we started with
     */
    public String removeWhitespace(String strValue)
    {
    	if (strValue==null)
    		return null;
    	return strValue.replaceAll("[ _-]", "");
    }

    

    /**
    * Parses the input, returns an integer corresponding to the supplied input using the radix
    * hex numbers can be formatted: 1234ah or 0x1234a, binary must be 011b, decimal must be 12345
    * Truncates input at 255 characters
    * 
    * @param rawInput	input to convert to an integer
    * @return null if it's not valid integer, otherwise the integer value is returned
    * @throws InvalidInputException 
    */
   public Integer convertToInteger(String rawInput,int  radix) 
   {
   	if ((rawInput==null)||(rawInput.length()==0) )
   		return null;
   	
   	if (rawInput.length()>255)
   		rawInput=rawInput.substring(0, 255);
   	
   	 Integer value=null;
      	 try 
      	 {
      		 if (radix==2)
      		 {
      			 if (rawInput.endsWith("b"))
      				 rawInput=rawInput.substring(0, rawInput.length()-1);

      			 //byte[] vals=DatatypeConverter.parseBase64Binary(rawInput);
      			 value=Integer.parseInt(rawInput, radix);
      		 }
      		 else if ((radix==10)||(radix==16))
      		 {
      			 if (rawInput.startsWith("0x"))
      				 rawInput=rawInput.substring(2, rawInput.length());
      			 if (rawInput.endsWith("h"))
      				 rawInput=rawInput.substring(0, rawInput.length()-1);
      			 value=Integer.parseInt(rawInput, radix);
      		 }
      		 else
      		 {
      			buildError(null, "Error converting: "+rawInput+" with invalid radix: "+radix);
      		 }   		 
      	 } 
      	 catch (IllegalArgumentException exc)
      	 {
   			buildError(null, "Error converting: "+rawInput+" to radix: "+radix);
   	    	//if (invInpExcep!=null) throw invInpExcep;

      	 }
      	 catch (Exception e)
      	 {
      		 return null;
      	 }
      	 return value;
   }
  

   /**
    * Parses the input, returns a Float corresponding to the supplied input
    * Truncates input at 255 characters
    * 
    * @param 	rawInput	Input to convert to a float
    * @return null if it's not valid float, the float value if it is
    * @throws InvalidInputException 
    */
   public Float convertToFloat(String rawInput) 
   {
   	if ((rawInput==null)||(rawInput.length()==0) )
   		return null;
   	if (rawInput.length()>255)
   		rawInput=rawInput.substring(0, 255);
   	
   	 Float value=null;
      	 try 
      	 {
      		 //byte[] vals=DatatypeConverter.parseBase64Binary(rawInput);
      		 value=Float.parseFloat(rawInput);
      	 } 
      	 catch (IllegalArgumentException exc)
      	 {
   			buildError(null, "Error converting: "+rawInput+" to float");
   	    	//if (invInpExcep!=null) throw invInpExcep;

      	 }
      	 return value;
   }
   
   
   
   /**
    * There may be multiple problems with an input and we'd like to see all the errors at once.
    * This appends error messages to an InvalidInputException in advance of it being thrown
    * 
    * @param 	cf	The customfield with the problem
    * @param 	errmsg	The error message to be appended to the list
    */
   public void buildError(CustomField cf, String errmsg) 
   {
   	// error getting the CF to be checked
      if (cf==null) 
      {
          if (invInpExcep==null) 
       	   invInpExcep = new InvalidInputException(errmsg);
          else 
       	   invInpExcep.addError(errmsg);
      }
      else 
      {
          if (invInpExcep==null) 
       	   invInpExcep = new InvalidInputException("Customfield "+ cf.getId(),cf.getName()+ " is missing but is required.");
          else 
       	   invInpExcep.addError("Customfield "+ cf.getId(),cf.getName()+ " is missing and is required.");
      }
  } // proc
   
   
   /**
    * checks whether the supplied input is a valid regexp. 
    * 
    * @param 	regexpinp 	input string to check
    * @return true if valid, false otherwise
    */
   public boolean isRegExpValid(String regexpinp)
   {
	   if (regexpinp==null)
		   return false;
		   
	   try {
   		Pattern.compile(regexpinp);
   		} catch (PatternSyntaxException exception) 
   		{
   			return false;
   		}
   		return true;
   }
   
   
   /**
    * Is this String a valid integer or not? null or "" are considered invalid integers
    * 
    * @param 	inString	input string
    * @return True if it's a valid integer, false otherwise. 
    */
   public boolean isValidInteger(String inString)
   {
	   int radix=10;
   		try {
   			Integer value=Integer.parseInt(inString, radix);
   			return true;
   		} 
     	 catch (IllegalArgumentException exc)
     	 {
  			return false;
     	 }
   }
   
   
   /**
    * Given a map of inputs from a form return the field of interest as the object type specified
    * 
    * @param 	args	Values from the velocity form
    * @param	fieldname	The name of the field	
    * @param 	rettype		The type of the object we want back (Boolean, String)	
    * @return null if there was invalid input, an error or other
    */
   public Object getFormData(Map args,String fieldname, Object rettype)
   {
   	if (args==null||fieldname==null||fieldname.length()==0||rettype==null)
   		return null;
   	
   	String data=(String)args.get(fieldname);
   	if (data!=null)
   	{
   		if (rettype instanceof String)
   			return data;
   		else if (rettype instanceof Boolean)
   		{
   			if (data.toLowerCase().indexOf("on")>-1)
   				return new Boolean(true);
   			else
   				return new Boolean(false);
   		}
   		
   	}
   	return null;
   }
	
   
   /**
    * Is the provided string valid xhtml or not? Just ask Jsoup.
    * The relaxed whitelist is used so the following are valid:
    * 	a, b, blockquote, br, caption, cite, code, col, colgroup, dd, 
    * dl, dt, em, h1, h2, h3, h4, h5, h6, i, img, li, ol, p, pre, q, 
    * small, strike, strong, sub, sup, table, tbody, td, tfoot, th, thead, 
    * tr, u, ul
    * 
    * @param strValue input String to be checked
    * @return true if the input is valid xhtml. Null is invalid as is "".
    */
   public boolean isXhtmlValid(String strValue)
   {
	  if ((strValue==null)||(strValue.length()==0) )
		  return false;
	  String cleaned=Jsoup.clean(strValue, Whitelist.relaxed());
	  if (cleaned.length()==0)
		  return false;
	  return Jsoup.isValid(cleaned, Whitelist.relaxed());
   }
   
} //class
