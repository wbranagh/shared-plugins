/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright (C) 2014  Wayne Branagh
*/
package com.wbranagh.jira.plugins;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import java.util.*;
import org.jfree.util.Log;

/**
 * Factory to create the plugin and pass configuration parameters to/from the UI (Velocity)
 * 
 * @author Wayne Branagh
 */
public class NumberFormatsFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory
{
    private final CustomFieldManager customFieldManager;
        
    final String DEFAULT_CHECKDECIMAL="on";
    final String DEFAULT_MINRANGE="0";
    final String DEFAULT_MAXRANGE="0";
    final String DEFAULT_CHECKCANBENULL="on";

    /**
     * Constructor
     * 
     * @param constantsManager
     */
    public NumberFormatsFactory(ConstantsManager constantsManager)
    {
        this.customFieldManager = ComponentAccessor.getCustomFieldManager();
    }


    /**
     * Parameters passed in for input for example defaults.
     * See NumberFormats-validator-input.vm
     * @see com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory#getVelocityParamsForInput(java.util.Map)
     * 
     * @param	Map	velocityParams	- defaults passed to the velocity template for initial input (when a validator is added to a workflow)
     */
    protected void getVelocityParamsForInput(Map<String,Object> velocityParams)
    {
    	Log.debug("GetVelocityParamsForInput");
    	if (velocityParams==null)
    		throw new IllegalArgumentException("VelocityParams input must not be null for input.");
    	
    	Log.debug("Setting params");
    	// get the list of customfields for selection in the UI
    	velocityParams.put("customFieldNameList", customFieldManager.getCustomFieldObjects()); 
    	velocityParams.put("checkDecimal", DEFAULT_CHECKDECIMAL);
    	 velocityParams.put("currentinpRangeMin", DEFAULT_MINRANGE);
    	 velocityParams.put("currentinpRangeMax", DEFAULT_MAXRANGE);
    	 velocityParams.put("checkCanBeNull", DEFAULT_CHECKCANBENULL);
    	 Log.debug("GetVelocityParamsForInput - done");
    }


    /**
     * Previously set parameters are passed in for update
     *  See NumberFormats-validator-edit.vm (eg. $currentcustomFieldName passed into velocity and displayed
     *  gets set here
     *  
     * @param	Map<String,Object>	velocityParams	values going to the velocity template
     * @param	AbstractDescriptor	descriptor	Validator parameters.
     */
    protected void getVelocityParamsForEdit(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
      Log.debug("getVelocityParamsForEdit");
      if(!(descriptor instanceof ValidatorDescriptor))
      {
          throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor." + descriptor);
      } 
      else  if (velocityParams==null)
    	  throw new IllegalArgumentException("VelocityParams input must not be null for editing.");

      else
      {
    	  Log.debug("GetVelocityParamsForEdit - settings params");
          ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor)descriptor;
      	  // get the list of customfields for selection in the UI
      	  velocityParams.put("customFieldNameList", customFieldManager.getCustomFieldObjects()); 
      	  String customFieldNameId = (String)validatorDescriptor.getArgs().get("field.customFieldNameId");
          velocityParams.put("currentcustomFieldNameId", customFieldNameId);
          	Log.debug("currentcustomFieldNameId "+ customFieldNameId);
          velocityParams.put("currentcustomFieldName", getFieldNameFromID(customFieldNameId));
          
          String val=(String)validatorDescriptor.getArgs().get("field.checkDecimal");
          velocityParams.put("currentcheckDecimal", "off");
          if ((val!=null))
        	  velocityParams.put("currentcheckDecimal", validatorDescriptor.getArgs().get("field.checkDecimal"));

          val=(String)validatorDescriptor.getArgs().get("field.checkFloating");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckFloating", validatorDescriptor.getArgs().get("field.checkFloating"));
          
          val=(String)validatorDescriptor.getArgs().get("field.checkNegative");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckNegative", validatorDescriptor.getArgs().get("field.checkNegative"));
          
          val=(String)validatorDescriptor.getArgs().get("field.checkHex");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckHex", validatorDescriptor.getArgs().get("field.checkHex"));
          
          val=(String)validatorDescriptor.getArgs().get("field.checkBin");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckBin", validatorDescriptor.getArgs().get("field.checkBin"));
          
          val=(String)validatorDescriptor.getArgs().get("field.checkRange");
          if ((val!=null)&&(val.indexOf("on")==0))
           	  velocityParams.put("currentcheckRange", validatorDescriptor.getArgs().get("field.checkRange"));
          
          velocityParams.put("currentinpRangeMin", validatorDescriptor.getArgs().get("field.inpRangeMin"));
    	  velocityParams.put("currentinpRangeMax", validatorDescriptor.getArgs().get("field.inpRangeMax"));
    	  
          val=(String)validatorDescriptor.getArgs().get("field.checkIgnoreWhitespace");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckIgnoreWhitespace", validatorDescriptor.getArgs().get("field.checkIgnoreWhitespace"));

          val=(String)validatorDescriptor.getArgs().get("field.checkCanBeNull");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckCanBeNull", validatorDescriptor.getArgs().get("field.checkCanBeNull"));

          
          Log.debug("GetVelocityParamsForEdit - settings params done");
          return;
      }
    }



    /**
     * When viewing the parameters for the condition, pass in values to velocity.
     * ValidatorDescriptor comes from the dbase.
     * velocityParams used in the display
     * 
     * @param	Map<String,Object>	velocityParams	values going to the velocity template
     * @param	AbstractDescriptor	descriptor	Validator parameters.
     */
    protected void getVelocityParamsForView(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
    	Log.debug("getVelocityParamsForView - start");
      if(!(descriptor instanceof ValidatorDescriptor))
          throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor." + descriptor);
      
      if (velocityParams==null)
    	  throw new IllegalArgumentException("VelocityParams input must not be null for viewing.");
      
      Log.debug("getting parameters");
      ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor)descriptor;
      
      String customFieldNameId = (String)validatorDescriptor.getArgs().get("field.customFieldNameId");
      velocityParams.put("customFieldNameId", customFieldNameId);
      velocityParams.put("customFieldName", getFieldNameFromID(customFieldNameId));
      
      String checkDecimal=(String) validatorDescriptor.getArgs().get("field.checkDecimal");
      velocityParams.put("checkDecimal", "off");
      if (checkDecimal!=null)
    	  velocityParams.put("checkDecimal", checkDecimal);
      
      String checkFloating=(String) validatorDescriptor.getArgs().get("field.checkFloating");
      velocityParams.put("checkFloating", "off");
      if (checkFloating!=null)
    	  velocityParams.put("checkFloating", checkFloating);
      
      String checkNegative=(String) validatorDescriptor.getArgs().get("field.checkNegative");
      velocityParams.put("checkNegative", "off");
      if (checkNegative!=null)
    	  velocityParams.put("checkNegative", checkNegative);

      String checkHex=(String) validatorDescriptor.getArgs().get("field.checkHex");
      velocityParams.put("checkHex", "off");
      if (checkHex!=null)
    	  velocityParams.put("checkHex", checkHex);
      
      String checkBin=(String) validatorDescriptor.getArgs().get("field.checkBin");
      velocityParams.put("checkBin", "off");
      if (checkBin!=null)
    	  velocityParams.put("checkBin", checkBin);
      
      String checkRange=(String) validatorDescriptor.getArgs().get("field.checkRange");
      velocityParams.put("checkRange", "off");
      if (checkRange!=null)
    	  velocityParams.put("checkRange", checkRange);

      String inpRangeMin=(String) validatorDescriptor.getArgs().get("field.inpRangeMin");
      velocityParams.put("inpRangeMin", "0");
      if (inpRangeMin!=null)
    	  velocityParams.put("inpRangeMin", inpRangeMin);
   	  
	  String inpRangeMax=(String) validatorDescriptor.getArgs().get("field.inpRangeMax");
	  velocityParams.put("inpRangeMax", "0");
      if (inpRangeMax!=null)
    	  velocityParams.put("inpRangeMax", inpRangeMax);
    
      String checkIgnoreWhitespace=(String) validatorDescriptor.getArgs().get("field.checkIgnoreWhitespace");
      velocityParams.put("checkIgnoreWhitespace", "off");
      if (checkIgnoreWhitespace!=null)
    	  velocityParams.put("checkIgnoreWhitespace", checkIgnoreWhitespace);

      String checkCanBeNull=(String) validatorDescriptor.getArgs().get("field.checkCanBeNull");
      velocityParams.put("checkCanBeNull", "off");
      if (checkCanBeNull!=null)
    	  velocityParams.put("checkCanBeNull", checkCanBeNull);

      Log.debug("getVelocityParamsForView - end");
    } // proc


    /**
     * Pull the parameter values back into Java from Velocity (user-input)
     * 
     */
    public Map<String,Object> getDescriptorParams(Map<String,Object> conditionParams)
    {
    	Log.debug("getDescriptorParams - start");
    	Map<String,Object> params = new HashMap<String,Object>();
    	if (conditionParams==null) // nothing to do
    	  return params;
      
      String customFieldNameId=extractSingleParam(conditionParams, "customFieldNameId");
      params.put("field.customFieldNameId", customFieldNameId);
      params.put("field.customFieldName", getFieldNameFromID(customFieldNameId));
      
      params.put("field.checkDecimal", "off");
      if (conditionParams.containsKey("checkDecimal"))
          	  params.put("field.checkDecimal", "on");
       
      params.put("field.checkFloating", "off");
      if (conditionParams.containsKey("checkFloating"))
    	  params.put("field.checkFloating","on");
      
      params.put("field.checkNegative", "off");
      if (conditionParams.containsKey("checkNegative"))
    	  params.put("field.checkNegative", "on");
      
      params.put("field.checkHex", "off");
      if (conditionParams.containsKey("checkHex"))
    	  params.put("field.checkHex", "on");
      
      params.put("field.checkBin", "off");
      if (conditionParams.containsKey("checkBin"))     
    	  params.put("field.checkBin", "on");
      
      params.put("field.checkRange", "off");
      if (conditionParams.containsKey("checkRange"))
    	  params.put("field.checkRange", "on");
      
      params.put("field.inpRangeMin", "0");
      if (conditionParams.containsKey("inpRangeMin"))
    	  params.put("field.inpRangeMin", extractSingleParam(conditionParams, "inpRangeMin"));
      params.put("field.inpRangeMax", "0");
      if (conditionParams.containsKey("inpRangeMax"))
    	  params.put("field.inpRangeMax", extractSingleParam(conditionParams, "inpRangeMax"));
      
      params.put("field.checkIgnoreWhitespace", "off");
      if (conditionParams.containsKey("checkIgnoreWhitespace"))
    	  params.put("field.checkIgnoreWhitespace", "on");

      params.put("field.checkCanBeNull", "off");
      if (conditionParams.containsKey("checkCanBeNull"))
    	  params.put("field.checkCanBeNull", "on");

      Log.debug("getDescriptorParams - end");
      return params;
    }

    
    /**
     * Given a CF ID, return the name. 
     * Why not just use names instead of IDs? CF Names don't have to be unique in Jira.
     * 
     * @param customFieldId  id of the customfield whose name you want
     * @return String  display name of the field
     */
    protected String getFieldNameFromID(String customFieldId)
    {
    	if ((customFieldId==null)||(customFieldId.length()<1))
    		return null;
    	
        List<CustomField> fields=customFieldManager.getCustomFieldObjects();
        for (int a=0;a<fields.size();a++)
        {
      	  CustomField cf=fields.get(a);
      	  if (cf.getId().indexOf(customFieldId)==0)
      		  return cf.getName(); 
        }
    	return null;
    }
} // class