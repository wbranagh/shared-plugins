/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright (C) 2014  Wayne Branagh
*/
package com.wbranagh.jira.plugins;

import com.opensymphony.workflow.Validator;
import com.opensymphony.module.propertyset.PropertySet;
import java.util.Map;
import com.opensymphony.workflow.InvalidInputException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.wbranagh.jira.plugins.PluginUtility;


/**
 * Implementation of a number check validator. The validator allows you to require decimal or 
 * floating point, negative or positive numbers, null is valid input, hex or binary format 
 * 
 * @author Wayne Branagh
 *
 */
public class NumberFormats implements Validator
{
    Issue issue=null;
    private PluginUtility util=new PluginUtility();
    
    public NumberFormats() 
    {
    }
    
    /**
     * Implements the validator logic
     * 
     * @param 	transientVars - populated by Jira and contains things like the current issue
     * @param 	args 	values from the input form. eg. the Params user set for the validator
     * @param 	ps	
     * 
     * @throws	InvalidInputException	If the issue values aren't valid, add a message and throw this.
     */
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException
    {
    	
    	if (transientVars==null||args==null||ps==null)
    	{
    		util.buildError(null, "Internal error in validate. Missing some parameters in the call.");
    		if (util.invInpExcep!=null) throw util.invInpExcep;
    	}
    	
    	CustomFieldManager customFieldManager= ComponentAccessor.getCustomFieldManager();
    	Issue currIssue = (Issue) transientVars.get("issue");
    	if (currIssue==null)
    	{
    		util.buildError(null, "Internal error in validate. No issue handle.");
    		if (util.invInpExcep!=null) throw util.invInpExcep;
    	}
  
        String strMinvalue="";
        String strMaxvalue="";

    	Boolean bRettype=new Boolean("true");
    	String cfNameId=(String)util.getFormData(args,"field.customFieldNameId", "");
    	Object objValue=util.getCustomFieldValueFromIssueById(currIssue,cfNameId);
    	
    	String cfName=util.getFieldNameFromID(customFieldManager,cfNameId);
    	
    	// check the type
    	String strValue=null;
    	if (objValue instanceof java.lang.Long)
    		strValue=((Long)objValue).toString();
    	else if (objValue instanceof java.lang.Integer)
    		strValue=((Integer)objValue).toString();
    	else
    		strValue=(String)objValue;
    	
    	// design choice - valid value is null. Can have  
    	Boolean checkCanBeNull=(Boolean)util.getFormData(args,"field.checkCanBeNull",bRettype);
    	if (strValue==null)
    	{
    		if (checkCanBeNull.booleanValue())
    			return; //all is well. Nothing expected. Nothing there.
    		else
    			util.buildError(null, "Error: Expected a value for "+cfName+". Nothing entered.");
    	}
    		
    	Boolean checkDecimal=(Boolean)util.getFormData(args,"field.checkDecimal",bRettype);
    	Boolean checkHexadecimal=(Boolean)util.getFormData(args,"field.checkHexadecimal",bRettype);
    	Boolean checkBinary=(Boolean)util.getFormData(args,"field.checkBinary",bRettype);
    	Boolean checkRange=(Boolean)util.getFormData(args,"field.checkRange",bRettype);
    	if (checkRange && checkRange.booleanValue())
    	{
    		strMinvalue=(String)util.getFormData(args,"field.inpMinvalue", "");
    		strMaxvalue=(String)util.getFormData(args,"field.inpMaxvalue", "");		
    	}
    	Boolean checkIgnoreWhitespace=(Boolean)util.getFormData(args,"field.checkIgnoreWhitespace",bRettype);
    	Boolean checkFloatingPoint=(Boolean)util.getFormData(args,"field.checkFloating",bRettype);

    	Boolean checkNegative=(Boolean)util.getFormData(args,"field.checkNegative",bRettype);
    	
        if (checkIgnoreWhitespace.booleanValue())
        	 strValue=util.removeWhitespace(strValue);
         
         boolean isValidNumber=false;
         if ((checkDecimal!=null)&&(checkDecimal.booleanValue()))
         {
        	 int radix=10;
        	 if (checkFloatingPoint)
        	 {
            	 Float result=util.convertToFloat(strValue);
            	 if (result!=null) // valid binary
            	 {
                	 if (checkRange)
                		 isValidNumber|=checkRange(result,strMinvalue,strMaxvalue,radix);
                	 else
                		 isValidNumber=true;
            	 } // if        		 
        	 } // if
        	 else
        	 {
            	 Integer result=util.convertToInteger(strValue,radix);
            	 if (result!=null) // valid binary
            	 {
                	 if (checkRange)
                		 isValidNumber|=checkRange(result,strMinvalue,strMaxvalue,radix);
                	 else
                		 isValidNumber=true;
            	 } // if        		 
        	 }
        			 
         }
         
         if ((checkHexadecimal!=null)&&(checkHexadecimal.booleanValue()) )
         {
        	 int radix=16;
        	 Integer result=util.convertToInteger(strValue,radix);
        	 if (result!=null) // valid binary
        	 {
            	 if (checkRange)
            		 isValidNumber|=checkRange(result,strMinvalue,strMaxvalue,radix);
            	 else
            		 isValidNumber=true;
        	 } // if        		 

         }
         
         else if ((checkBinary!=null)&&(checkBinary.booleanValue()) )
         {
        	 int radix=2;
        	 Integer result=util.convertToInteger(strValue,radix);
        	 if (result!=null) // valid binary
        	 {
            	 if (checkRange)
            		 isValidNumber|=checkRange(result,strMinvalue,strMaxvalue,radix);
            	 else
            		 isValidNumber=true;
        	 } // if        		 
         } // binary
         
         if (!isValidNumber)
         {
        	 util.buildError(null, "Error: Validation on number value: "+strValue+" for field:"+cfName+" failed.");
        	 if (util.invInpExcep!=null) throw util.invInpExcep;
         }
        	 
    } // proc

    
    
    /**
     * Given an integer or Float, and a range, return whether the number is in the range.
     * Integers can be binary, decimal or hex numbers. The ranges need to have the same radix.
     * Ranges are inclusive.
     * 
     * @param 	Object	result	Integer or Float under consideration
     * @param 	String	strMinvalue	Lower bound of the range
     * @param 	String 	strMaxvalue	Upper bound of the range	
     * @param 	int		radix	Are we dealing with base 2, 10 or 16?
     * @return 	Boolean
     */
	 protected Boolean checkRange(Object result, String strMinvalue, String strMaxvalue,int radix)
	 {
		 Boolean FALSE=new Boolean(false);
		 Boolean TRUE=new Boolean(true);
		 
		 if (result==null||strMinvalue==null||strMaxvalue==null)
			 return FALSE;
		 
		 if (radix<2)
			 return FALSE;
		 
		 if (result instanceof Integer)
		 {
			 Integer min=util.convertToInteger(strMinvalue,radix);
			 Integer max=util.convertToInteger(strMaxvalue,radix);
			 if ((min==null)||(max==null))
			 {
				 util.buildError(null, "Error: either min or max wasn't specified in the range check.");
				 return FALSE;
	    	// if (invInpExcep!=null) throw invInpExcep;
			 } 
			 if (((Integer)result).longValue()>=min.longValue()&&(((Integer)result).longValue()<=max.longValue()) )
			 	return TRUE;
		 }
		 
		 else if (result instanceof Float)
		 {
			 Float min=util.convertToFloat(strMinvalue);
			 Float max=util.convertToFloat(strMaxvalue);
			 if ((min==null)||(max==null))
			 {
				 util.buildError(null, "Error: either min or max wasn't specified in the range check.");
				 return FALSE;
	    	// if (invInpExcep!=null) throw invInpExcep;
			 } 
			 if (((Float)result).floatValue()>=min.doubleValue()&&(((Float)result).floatValue()<=max.doubleValue()) )
			 	return TRUE;
		 }
			
		 return FALSE;
	 }
	 


    
} // class
