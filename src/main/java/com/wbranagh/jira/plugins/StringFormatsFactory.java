/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright (C) 2014  Wayne Branagh
*/

package com.wbranagh.jira.plugins;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import java.util.*;

/**
 * Factory to create the plugin and pass configuration parameters to/from the UI (Velocity)
 * 
 * @author Wayne Branagh
 *
 */
public class StringFormatsFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory
{
    private final CustomFieldManager customFieldManager;
    final String DEFAULT_REGEXP="*";
    final String DEFAULT_CHECKCANBENULL="on";
    private PluginUtility util=new PluginUtility();
    
    /**
     * 
     * @param constantsManager
     */
    public StringFormatsFactory(ConstantsManager constantsManager)
    {
        this.customFieldManager = ComponentAccessor.getCustomFieldManager();
    }


    /**
     * Parameters passed in for input. Defaults, etc.
     * See NumberFormats-validator-input.vm for how these are displayed
     * 
     * @param	Map	velocityParams	- defaults passed to the velocity template for initial input (when a validator is added to a workflow)
     * 
     * @see com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory#getVelocityParamsForInput(java.util.Map)
     */
    protected void getVelocityParamsForInput(Map<String,Object> velocityParams)
    {
    	if (velocityParams==null)
    		throw new IllegalArgumentException("VelocityParams input must not be null for input.");
    	
    	// get the list of customfields for selection in the UI
    	velocityParams.put("customFieldNameList", customFieldManager.getCustomFieldObjects()); 
    	velocityParams.put("currentinpRegExp", DEFAULT_REGEXP);
    	velocityParams.put("checkCanBeNull", DEFAULT_CHECKCANBENULL);
    }


    /**
     * Parameters passed in for edit. 
     *  See NumberFormats-validator-edit.vm (eg. $currentcustomFieldName passed into velocity and displayed
     *  gets set here
     *  
     * @param	Map<String,Object>	velocityParams	values going to the velocity template
     * @param	AbstractDescriptor	descriptor	Validator parameters.
     * 
     * @see com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory#getVelocityParamsForEdit(java.util.Map, com.opensymphony.workflow.loader.AbstractDescriptor)
     */
    protected void getVelocityParamsForEdit(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
      if(!(descriptor instanceof ValidatorDescriptor))
      {
          throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor." + descriptor);
      } 
      else  if (velocityParams==null)
    	  throw new IllegalArgumentException("VelocityParams input must not be null for editing.");

      else
      {
          ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor)descriptor;
        	// get the list of customfields for selection in the UI
        	velocityParams.put("customFieldNameList", customFieldManager.getCustomFieldObjects()); 
        	
        	String customFieldNameId = (String)validatorDescriptor.getArgs().get("field.customFieldNameId");
            velocityParams.put("currentcustomFieldNameId", customFieldNameId);

            String val=(String)validatorDescriptor.getArgs().get("field.checkXhtml");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckXhtml", validatorDescriptor.getArgs().get("field.checkXhtml"));

          val=(String)validatorDescriptor.getArgs().get("field.checkCanBeNull");
          if ((val!=null)&&(val.indexOf("on")==0))
        	  velocityParams.put("currentcheckCanBeNull", validatorDescriptor.getArgs().get("field.checkCanBeNull"));
          
          velocityParams.put("currentinpRegExp", validatorDescriptor.getArgs().get("field.inpRegExp"));
          return;
      }
    }



    /**
     * When viewing the parameters for the condition, pass in values to velocity.
     * 
     * @param	Map<String,Object>	velocityParams	values going to the velocity template
     * @param	AbstractDescriptor	descriptor	Validator parameters.
     * 
     * @see com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory#getVelocityParamsForView(java.util.Map, com.opensymphony.workflow.loader.AbstractDescriptor)
     */
    protected void getVelocityParamsForView(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
      if(!(descriptor instanceof ValidatorDescriptor))
          throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor." + descriptor);
      
      if (velocityParams==null)
    	  throw new IllegalArgumentException("VelocityParams input must not be null for viewing.");
      
      ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor)descriptor;
      
      String customFieldNameId = (String)validatorDescriptor.getArgs().get("field.customFieldNameId");
      velocityParams.put("customFieldNameId", customFieldNameId);
      velocityParams.put("customFieldName", util.getFieldNameFromID(customFieldManager,customFieldNameId));
      
      String checkXhtml=(String) validatorDescriptor.getArgs().get("field.checkXhtml");
      velocityParams.put("checkXhtml", "off");
      if (checkXhtml!=null)
    	  velocityParams.put("checkXhtml", checkXhtml);
      
      String inpRegExp=(String) validatorDescriptor.getArgs().get("field.inpRegExp");
      velocityParams.put("inpRegExp", inpRegExp);

      String checkCanBeNull=(String) validatorDescriptor.getArgs().get("field.checkCanBeNull");
      velocityParams.put("checkCanBeNull", "off");
      if (checkCanBeNull!=null)
    	  velocityParams.put("checkCanBeNull", checkCanBeNull);
    } // proc


    /**
     * Pull the parameter values back into Java from Velocity (user-input)
     * 
     * @see com.atlassian.jira.plugin.workflow.WorkflowPluginFactory#getDescriptorParams(java.util.Map)
     */
    public Map<String,Object> getDescriptorParams(Map<String,Object> conditionParams)
    {
      Map<String,Object> params = new HashMap<String,Object>();
      if (conditionParams==null) // nothing to do
    	  return params;
      
      String customFieldNameId=extractSingleParam(conditionParams, "customFieldNameId");
      params.put("field.customFieldNameId", customFieldNameId);
      params.put("field.customFieldName", util.getFieldNameFromID(customFieldManager,customFieldNameId));
      
      params.put("field.checkXhtml", "off");
      if (conditionParams.containsKey("checkXhtml"))
          	  params.put("field.checkXhtml", "on");
      params.put("field.inpRegExp", extractSingleParam(conditionParams, "inpRegExp"));

      if (conditionParams.containsKey("checkCanBeNull"))
      	  params.put("field.checkCanBeNull", "on");
      return params;
    }

    
} // class