/**
 * 
 */
package ut.com.wbranagh.jira.plugins;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.wbranagh.jira.plugins.NumberFormats;
import com.wbranagh.jira.plugins.PluginUtility;

/**
 * @author BoBo
 *
 */
public class UtilityTest {

	Class clas = NumberFormats.class;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	
	/**
	 * Test method for {@link com.wbranagh.jira.plugins.NumberFormats#removeWhitespace(java.lang.String)}.
	 */
	@Test
	public void testRemoveWhitespace() 
	{
		PluginUtility util = new PluginUtility();
		try 
		{
			String emptString=new String(""); 
			String result=(String)util.removeWhitespace(emptString);	
			assertTrue(result.length()==0);

			result=(String)util.removeWhitespace(null);	
			assertTrue(result==null);

			String k=null;
			result=(String)util.removeWhitespace(k);	
			assertTrue(result==null);

			result=(String)util.removeWhitespace( "----_____    -----");	
			assertTrue(result!=null);
			assertTrue(result.length()==0);
		
			result=(String)util.removeWhitespace( " 1 2 3 4 5 6 7--8 - 9 -__0  -----");	
			assertTrue(result!=null);
			assertTrue(result.length()==10);
			
			result=util.removeWhitespace("abc654");
			assertTrue(result.length()==6);
			
		}
		catch(Exception eee)
		{
			fail("Exception encountered during test. "+eee.toString());
		}
	
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#convertToInteger(java.lang.String, int)}.
	 */
	@Test
	public void testConvertToInteger() 
	{
		PluginUtility util = new PluginUtility();
		try 
		{
			String k=null;
			Integer result=(Integer)util.convertToInteger(k ,10);	
			assertTrue(result==null);
			
			result=(Integer)util.convertToInteger( "",2);	
			assertTrue(result==null);

			result=(Integer)util.convertToInteger( "1",3);	// invalid radix
			assertTrue(result==null);
			
			result=(Integer)util.convertToInteger( "123",10);	
			assertTrue(result.intValue()==123);

			result=(Integer)util.convertToInteger( "-123",10);	
			assertTrue(result.intValue()==-123);

			result=(Integer)util.convertToInteger( "00ff",16);	
			assertTrue(result.intValue()==255);

			result=(Integer)util.convertToInteger( "0x00ff",16);	
			assertTrue(result!=null);
			assertTrue(result.intValue()==255);

			result=(Integer)util.convertToInteger( "00ffh",16);	
			assertTrue(result.intValue()==255);

			result=(Integer)util.convertToInteger( "badinput",10);	
			assertTrue(result==null);
			
			result=(Integer)util.convertToInteger( "00ff",10);	
			assertTrue(result==null);

			result=(Integer)util.convertToInteger( "0111",10);	
			assertTrue(result.intValue()==111);

			result=(Integer)util.convertToInteger( "0111b",2);	
			assertTrue(result.intValue()==7);

			result=(Integer)util.convertToInteger( "0119ab",2);	
			assertTrue(result==null);

		}
		
		catch(Exception eee)
		{
			fail("Exception encountered during test. "+eee.toString());
		}
		
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#convertToFloat(java.lang.String)}.
	 */
	@Test
	public void testConvertToFloat() 
	{
		
		PluginUtility util = new PluginUtility();

		String nullString=new String(); // can't invoke with a null.
		Float result=(Float)util.convertToFloat( nullString);	
		assertTrue(result==null);

		result=(Float)util.convertToFloat("");	
		assertTrue(result==null);

		result=(Float)util.convertToFloat( "-1ABC3.5");
		assertTrue(result==null);

		result=(Float)util.convertToFloat( "-11.5^3");
		assertTrue(result==null);
		

		result=(Float)util.convertToFloat( "123.5");
		assertTrue(result!=null);
		assertTrue( (result.floatValue()>123) && (result.floatValue()<124) );
			
		result=(Float)util.convertToFloat( "-123.5");
		assertTrue(result!=null);
		assertTrue( (result.floatValue()>-124) && (result.floatValue()<-123) );
			
		result=(Float)util.convertToFloat( "-1.233e3");
		assertTrue(result!=null);
		assertTrue( (result.floatValue()>-1240) && (result.floatValue()<-1230) );

		
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#buildError(com.atlassian.jira.issue.fields.CustomField, java.lang.String)}.
	 */
	@Test
	public void testBuildError() 
	{
		PluginUtility util=new PluginUtility();
		assertTrue(util.invInpExcep==null);
		
		util.buildError(null, null);
		assertTrue(util.invInpExcep!=null);

		util.buildError(null, "");
		assertTrue(util.invInpExcep!=null);

		util.buildError(null, "This is a message");
		assertTrue(util.invInpExcep!=null);
		
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#isRegExpValid(java.lang.String)}.
	 */
	@Test
	public void testIsRegExpValid() 
	{
		PluginUtility util=new PluginUtility();
		
		assertFalse(util.isRegExpValid(null));
		assertFalse(util.isRegExpValid("[a-z"));
		assertFalse(util.isRegExpValid("[a-z))"));

		assertTrue(util.isRegExpValid(""));
		assertTrue(util.isRegExpValid("[a-z]"));
		assertTrue(util.isRegExpValid("/[a-z]()+/"));
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#isValidInteger(java.lang.String)}.
	 */
	@Test
	public void testIsValidInteger() 
	{
		PluginUtility util=new PluginUtility();
		
		assertFalse(util.isValidInteger(null));
		assertFalse(util.isValidInteger(""));
		assertFalse(util.isValidInteger("1.1"));
		assertFalse(util.isValidInteger("abc"));
		
		assertTrue(util.isValidInteger("123"));
		assertTrue(util.isValidInteger("0"));
		assertTrue(util.isValidInteger("-653452"));
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.Utility#getFormData(java.util.Map, java.lang.String, java.lang.Object)}.
	 */
	@Test
	public void testGetFormData() {
		//fail("Not yet implemented");
	}

	
	
	@Test
	public void testgetCustomFieldValueFromIssueById() 
	{
		PluginUtility util = new PluginUtility();
		
		Issue i=null;
		Object o=util.getCustomFieldValueFromIssueById(i, null);
		assertTrue(o==null);

		o=util.getCustomFieldValueFromIssueById(i, "");
		assertTrue(o==null);

	}
	
	@Test
	public void testXhtmlValid()
	{
		PluginUtility util=new PluginUtility();
		assertFalse(util.isXhtmlValid(null));
		assertFalse(util.isXhtmlValid(""));
		assertFalse(util.isXhtmlValid("<htmlxx>"));
		assertFalse(util.isXhtmlValid("<badtext"));
		assertFalse(util.isXhtmlValid("<html></bad>"));
		
		assertTrue(util.isXhtmlValid("<a></a>"));
		assertTrue(util.isXhtmlValid("<a><table></table></a>"));
		
		
	}
}
