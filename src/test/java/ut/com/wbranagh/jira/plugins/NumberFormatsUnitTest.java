package ut.com.wbranagh.jira.plugins;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.component.*;


import org.junit.Before;
import org.junit.Test;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.wbranagh.jira.plugins.NumberFormats;

import static org.junit.Assert.*;

public class NumberFormatsUnitTest
{
	Class clas = NumberFormats.class;

	/*
    @Test
    public void testMyName()
    {
        NumberFormats component = new NumberFormats();
        assertEquals("names do not match!", "NumberFormats",component.getName());
    }
    */
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
	

	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.NumberFormats#NumberFormats(com.atlassian.sal.api.ApplicationProperties)}.
	 */
	@Test
	public void testNumberFormats() 
	{
		
		//ApplicationProperties mAP=(ApplicationProperties) new MockApplicationProperties();
		NumberFormats myNumberFormats = new NumberFormats();
		assertTrue(myNumberFormats!=null);
	}


	/**
	 * Test method for {@link com.wbranagh.jira.plugins.NumberFormats#getFormData(java.util.Map, java.lang.String, java.lang.Object)}.
	 */
	@Test
	public void testValidate() 
	{
		NumberFormats myNumberFormats = new NumberFormats();
		Map transientVars=new HashMap();
		Map args=new HashMap();
		PropertySet ps=null;
		
		try {
			myNumberFormats.validate(null, args, ps);
			fail("Should have thrown an exception");
		}catch (InvalidInputException iii)
		{ }
		
		try {
			myNumberFormats.validate(transientVars, null, ps);
			fail("Should have thrown an exception");
		}catch (InvalidInputException iii)
		{ }
		
		try {
			myNumberFormats.validate(transientVars, args, null);
			fail("Should have thrown an exception");
		}catch (InvalidInputException iii)
		{ }
		
	}


	/**
	 * Test method for {@link com.wbranagh.jira.plugins.NumberFormats#checkRange(java.lang.Object, java.lang.String, java.lang.String, int)}.
	 */
	@Test
	public void testCheckRange() 
	{
		NumberFormats myNumberFormats = new NumberFormats();

		try 
		{
			Method checkRange=clas.getDeclaredMethod("checkRange", Object.class,String.class, String.class, int.class);
			checkRange.setAccessible(true);	// extra effort for protected procs

			// parameter checks ints
			String empString=new String(""); // can't invoke with a null.
			Integer badval=null;
			Boolean result=(Boolean)checkRange.invoke(myNumberFormats, badval, "0", "4",10);	
			assertFalse(result.booleanValue());

			Integer ni=new Integer(2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "", "4",10);	
			assertFalse(result.booleanValue());
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, null, "4",10);	
			assertFalse(result.booleanValue());
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "0", null ,10);	
			assertFalse(result.booleanValue());
			
			//floating check parameters
			Float badvalf=null;
			result=(Boolean)checkRange.invoke(myNumberFormats, badvalf, "0", "4",10);	
			assertFalse(result.booleanValue());
			Float nif=new Float(2.0);
			result=(Boolean)checkRange.invoke(myNumberFormats, nif, "", "4",10);	
			assertFalse(result.booleanValue());
			result=(Boolean)checkRange.invoke(myNumberFormats, nif, null, "4",10);	
			assertFalse(result.booleanValue());
			result=(Boolean)checkRange.invoke(myNumberFormats, nif, "0", null ,10);	
			assertFalse(result.booleanValue());

			
			ni=new Integer(2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "2", 10);	//valid range
			assertTrue(result.booleanValue());

			nif=new Float(2.1);
			result=(Boolean)checkRange.invoke(myNumberFormats, nif, "1.0", "2.2", 10);	//valid range
			assertTrue(result.booleanValue());

			
			ni=new Integer(5);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "10", 1);	//invalid radix
			assertFalse(result.booleanValue());
			
			ni=new Integer(2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "3", "1", 10);	//invalid range
			assertFalse(result.booleanValue());

			nif=new Float(2.0);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "3.1", "1", 10);	//invalid range
			assertFalse(result.booleanValue());

			ni=new Integer(12);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "9", "B", 16);	//invalid range
			assertFalse(result.booleanValue());

			ni=new Integer(5);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "0111", "10000", 2);	//invalid range
			assertFalse(result.booleanValue());

			ni=new Integer(-2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "4", 10);	//invalid range
			assertFalse(result.booleanValue());

			ni=new Integer(2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "blah", "4", 10);	//invalid input
			assertFalse(result.booleanValue());
			
			// conversion tests
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "10", 10);	//valid conversion			
			assertTrue(result.booleanValue());
			
			ni=new Integer(2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "2", 10);	//valid range
			assertTrue(result.booleanValue());
			
			ni=new Integer(1);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "1", "2", 10);	//valid range
			assertTrue(result.booleanValue());
			
			ni=new Integer(10);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "9", "B", 16);	//valid range
			assertTrue(result.booleanValue());
			
			ni=new Integer(7);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "0110", "10000", 2);	//valid range
			assertTrue(result.booleanValue());
			
			ni=new Integer(-2);
			result=(Boolean)checkRange.invoke(myNumberFormats, ni, "-4", "-1", 10);	//valid range
			assertTrue(result.booleanValue());

			// floating
			nif=new Float(2.1);
			result=(Boolean)checkRange.invoke(myNumberFormats, nif, "1.0", "2.2", 10);	//valid range
			assertTrue(result.booleanValue());

			

			
		}
		catch (NoSuchMethodException e) {
			fail("Test case error. No such method "+e.toString());
		}
		catch (IllegalAccessException e) {
			fail("Test case error. Illegal access "+e.toString());
		}
		catch(InvocationTargetException eee)
		{
			fail("Invocation exception encountered during test. "+eee.toString());
		}
		
	}

	
	

	

}