/**
 * 
 */
package ut.com.wbranagh.jira.plugins;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.wbranagh.jira.plugins.StringFormats;

/**
 * @author BoBo
 *
 */
public class StringFormatsTest 
{
	StringFormats sf;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		sf=new StringFormats();
	}

	/**
	 * Test method for {@link com.wbranagh.jira.plugins.StringFormats#validate(java.util.Map, java.util.Map, com.opensymphony.module.propertyset.PropertySet)}.
	 */
	@Test
	public void testValidate() 
	{
		assert(sf!=null);
		Map m=new HashMap();
		try {
			sf.validate(null, null, null);
			fail("Expected InvalidInputException to be thrown");
        } catch (InvalidInputException eee) {
        	
	    }


	}

}
