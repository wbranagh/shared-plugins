package ut.com.wbranagh.jira.plugins;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ StringFormatsTest.class, NumberFormatsUnitTest.class,
		UtilityTest.class })
public class AllTests {

}
